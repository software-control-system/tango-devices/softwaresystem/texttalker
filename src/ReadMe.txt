========================================================================

       CONSOLE APPLICATION : test

========================================================================





AppWizard has created this test application for you.  



This file contains a summary of what you will find in each of the files that

make up your test application.



TextTalker.dsp

    This file (the project file) contains information at the project level and

    is used to build a single project or subproject. Other users can share the

    project (.dsp) file, but they should export the makefiles locally.



TextTalker.cpp

    This is the main application source file.





/////////////////////////////////////////////////////////////////////////////

Other standard files:



StdAfx.h, StdAfx.cpp

    These files are used to build a precompiled header (PCH) file

    named test.pch and a precompiled types file named StdAfx.obj.





/////////////////////////////////////////////////////////////////////////////

Other notes:



AppWizard uses "TODO:" to indicate parts of the source code you

should add to or customize.



/////////////////////////////////////////////////////////////////////////////





/////////////////////////////////////////////////////////////////////////////

Settings for the compilation with visual C++ :



At ESRF :

Z:\ corresponds to /segfs/



Additional include directories :

Z:\tango\include\win32,C:\Program Files\Microsoft Speech SDK 5.1\Include



LINK :

Objects/library modules :

kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  COS4.lib COSDynamic4.lib log4tango.lib omniDynamic4.lib omniORB4.lib omnithread.lib tango.4.0.lib sapi.lib winmm.lib ws2_32.lib comctl32.lib 



Additional library path :

Z:\tango\lib\win32\release,C:\Program Files\Microsoft Speech SDK 5.1\Lib\i386

