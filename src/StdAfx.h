// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//


#if !defined(AFX_STDAFX_H__6C7934EB_EAD1_42C0_8A3B_F78ECEE2EEAD__INCLUDED_)
#define AFX_STDAFX_H__6C7934EB_EAD1_42C0_8A3B_F78ECEE2EEAD__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <stdio.h>

#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override something, 
//but do not change the name of _Module

extern CComModule _Module;
#include <atlcom.h>


// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__6C7934EB_EAD1_42C0_8A3B_F78ECEE2EEAD__INCLUDED_)

